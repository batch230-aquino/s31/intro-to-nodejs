
// The "http module" let Node.js transfer data using Hyper Text Transfer Protol (http)
    // it can create an HTTP server that listens to server ports such as...
        // 3000,4000,5000,8000 (usually used to for web developement)
// the "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applicationc

const http =require("http");

let port = 4000;

const server = http.createServer((request, response) => {

    if (request.url == '/greeting'){
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Hello Mundo');
    }
    else if(request.url == '/homepage'){
            response.writeHead(200, {'Content-Type': 'text/plain'});
            response.end('This is homepage');
    }
    else{
        response.writeHead(404, {'Content-Type' : 'text/plain'});
        response.end('404 Page not available');
    }

}) 


server.listen(port)

console.log("Server now acessible at local host" + port);




   

