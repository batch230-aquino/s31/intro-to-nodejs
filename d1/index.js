//Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http module" let Node.js transfer data using Hyper Text Transfer Protol (http)
    // it can create an HTTP server that listens to server ports such as...
        // 3000,4000,5000,8000 (usually used to for web developement)
// the "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between application
//client (devices/browsers) and server (nodeJs/ExpressJS application) communicate by exchanging individual message request (request/response)
// Request - the message sent by the client
//Response- the message sent by the server as response

let http= require("http");

//using the module's createServer() method, we can create an HTTP server that listens to request on a specified port and gives responses back to thte client.
//createServer() is a method of the http object responsible for creating a server using Node.js


http.createServer(function(request, response){

    //use writeHead() method to :
    // set a status code for  the response - a 200 means OK
    //set the content-type of the response as a plain text message
    response.writeHead(200, {'Content-Type': 'text/plain'})

    //Send the response with the content 'Hello Mundo'
    response.end('Hello Mundo');

}) .listen(4000); // a port is a virtual point where network connections start and ends.
//the server will be assigned to port 4000 via listen() method.
    //where the server will listen to any request that  sent to it and will also sent the response via port 4000



// when server is running, console will print the message:
console.log('Server is running at localhost: 4000');
//the messages or outputs from using console.log will now be displayed to the terminal

